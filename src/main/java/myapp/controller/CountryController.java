package myapp.controller;

import myapp.model.Country;
import myapp.model.SubArea;
import myapp.service.interfaces.CountryService;
import myapp.service.interfaces.SubAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping("/country")
public class CountryController {

  @Autowired CountryService countryService;
    @Autowired
    SubAreaService subAreaService;

  @GetMapping
  public String test(Map<String, Object> model) {

    model.put("countries", countryService.get());
    model.put("subAreas", subAreaService.getDictionary());

    return "country";
  }

  @ResponseBody
  @PostMapping("/add")
  public String add(Country country,Integer subArea_id) {

    SubArea subArea=new SubArea();
    subArea.setId(subArea_id);

    country.setSubArea(subArea);
    countryService.save(country);

    return "ok";
  }

  @ResponseBody
  @PostMapping("/update")
  public String update(Country country,Integer subArea_id) {

    SubArea subArea=new SubArea();
    subArea.setId(subArea_id);

    country.setSubArea(subArea);
    countryService.update(country);

    return "ok";
  }
}
