package myapp.service.interfaces;

import myapp.model.Country;
import myapp.model.SubArea;

import java.util.List;

public interface CountryService {
    void save(Country country);
    void update(Country country);
    List<Country> getDictionary();
    List<Country>get();


}
