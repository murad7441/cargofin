package myapp.service.interfaces;

import myapp.model.Airport;
import java.util.List;

public interface AirportService {
    void save(Airport airport);
    void update(Airport airport);
    List<Airport>getDictionary();
    List<Airport>get();
}
