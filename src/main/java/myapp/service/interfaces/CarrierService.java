package myapp.service.interfaces;

import myapp.model.Carrier;

import java.util.List;

public interface CarrierService {
    void save(Carrier carrier);
    void update(Carrier carrier);
    List<Carrier> getDictionary();
    List<Carrier>get();
}
