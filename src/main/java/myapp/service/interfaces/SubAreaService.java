package myapp.service.interfaces;

import myapp.model.SubArea;

import java.util.List;

public interface SubAreaService {
    void save(SubArea subArea);
    void update(SubArea subArea);
    List<SubArea> getDictionary();
    List<SubArea>get();


}
