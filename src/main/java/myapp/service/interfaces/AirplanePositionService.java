package myapp.service.interfaces;

import myapp.model.AirplanePosition;

import java.util.List;

public interface AirplanePositionService {
    void save(AirplanePosition airplanePosition);
    void update(AirplanePosition airplanePosition);
    List<AirplanePosition>getDictionary();
    List<AirplanePosition>get();
}
