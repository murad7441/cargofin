package myapp.service.impl;

import myapp.model.Carrier;
import myapp.repo.interfaces.CarrierRepo;
import myapp.service.interfaces.CarrierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarrierServiceImpl implements CarrierService {

    @Autowired
    CarrierRepo carrierRepo;

    @Override
    public void save(Carrier carrier) {
        carrierRepo.save(carrier);
    }

    @Override
    public void update(Carrier carrier) {
        carrierRepo.update(carrier);
    }

    @Override
    public List<Carrier> getDictionary() {
        return carrierRepo.getDictionary();
    }

    @Override
    public List<Carrier> get() {
        return carrierRepo.get();
    }
}
