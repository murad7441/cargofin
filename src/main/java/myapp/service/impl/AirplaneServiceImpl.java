package myapp.service.impl;

import myapp.model.Airplane;
import myapp.repo.interfaces.AirplaneRepo;
import myapp.service.interfaces.AirplaneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AirplaneServiceImpl implements AirplaneService {

    @Autowired
    AirplaneRepo airplaneRepo;


    @Override
    public void save(Airplane airplane) {
       airplaneRepo.save(airplane);
    }

    @Override
    public void update(Airplane airplane) {
        airplaneRepo.update(airplane);
    }

    @Override
    public List<Airplane> getDictionary() {
        return airplaneRepo.getDictionary();
    }

    @Override
    public List<Airplane> get() {
        return airplaneRepo.get();
    }
}
