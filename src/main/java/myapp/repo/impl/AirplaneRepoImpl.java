package myapp.repo.impl;

import myapp.model.Airplane;
import myapp.model.Airport;
import myapp.repo.interfaces.AirplaneRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AirplaneRepoImpl implements AirplaneRepo {

    @Autowired
    JdbcTemplate jdbcTemplate;



    @Override
    public void save(Airplane airplane) {
        String saveSQL="INSERT INTO lb_airplanes(airplane_name, icao, iata, MTOW, MLW, MZFW, weight, max_width, max_length,wide_body) VALUES (?,?,?,?,?,?,?,?,?,?)";
        jdbcTemplate.update(saveSQL,airplane.getName(),airplane.getICAO(),airplane.getIATA(),airplane.getMTOW(),airplane.getMLW(),airplane.getMZFW(),airplane.getWeight(),airplane.getMaxWidth(),airplane.getMaxLength(),airplane.isWideBody());
    }

    @Override
    public void update(Airplane airplane) {
        String updateSQL="Update  lb_airplanes Set airplane_name=?, icao=?, iata=?, MTOW=?, MLW=?, MZFW=?, weight=?, max_width=?, max_length=?,wide_body=? WHERE id=?";
        jdbcTemplate.update(updateSQL,airplane.getName(),airplane.getICAO(),airplane.getIATA(),airplane.getMTOW(),airplane.getMLW(),airplane.getMZFW(),airplane.getWeight(),airplane.getMaxWidth(),airplane.getMaxLength(),airplane.isWideBody(),airplane.getId());

    }

    @Override
    public List<Airplane> getDictionary() {
        String selectSQL="Select id as id ,airplane_name as name from lb_airplanes";
        return jdbcTemplate.query(selectSQL,new BeanPropertyRowMapper(Airplane.class));
    }

    @Override
    public List<Airplane> get() {
        String selectSQL="Select id as id,airplane_name as name, icao as ICAO, iata as IATA, MTOW as MTOW, MLW as MLW, MZFW as MZFW, weight as weight, max_width as maxWidth, max_length as maxLength,wide_body as wideBody from lb_airplanes";

        return jdbcTemplate.query(selectSQL,new BeanPropertyRowMapper(Airplane.class));
    }
}
