package myapp.repo.impl;

import myapp.features.Utils;
import myapp.model.Country;
import myapp.model.SubArea;
import myapp.repo.interfaces.CountryRepo;
import myapp.repo.interfaces.SubAreaRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CountryRepoImpl implements CountryRepo {

  @Autowired JdbcTemplate jdbcTemplate;

  @Override
  public void save(Country country) {
    String saveSQL =
        "INSERT INTO lb_countries(cnt_code,cnt_prefix,cnt_name,currency_name,currency_code,iata_area_id) VALUES (?,?,?,?,?,?)";

    jdbcTemplate.update(
        saveSQL,
        country.getCode(),
        country.getPrefix(),
        country.getName(),
        country.getCurrencyName(),
        country.getCurrencyCode(),
        country.getSubArea().getId());
  }

  @Override
  public void update(Country country) {
    String updateSQL =
        "UPDATE lb_countries SET  cnt_code=?,cnt_prefix=?,cnt_name=?,currency_name=?,currency_code=?,iata_area_id=? WHERE id=?";
    jdbcTemplate.update(
        updateSQL,
        country.getCode(),
        country.getPrefix(),
        country.getName(),
        country.getCurrencyName(),
        country.getCurrencyCode(),
        country.getSubArea().getId(),
        country.getId());
  }

  @Override
  public List<Country> getDictionary() {
    String selectSQL = "SELECT id as id,cnt_name as name from lb_countries";
    return jdbcTemplate.query(selectSQL, new BeanPropertyRowMapper(Country.class));
  }

  @Override
  public List<Country> get() {
    String selectSQL =
        "SELECT  lb_countries.id as id,a.id as subArea_id, cnt_code as code,cnt_prefix as prefix ,cnt_name as name,currency_name as currencyName,currency_code as currencyCode,a.iata_sub_area_name as subArea,iata_area_code as iataAreaCode from lb_countries LEFT JOIN lb_sub_area a on lb_countries.iata_area_id = a.id";

    List<Country> countries = new ArrayList<Country>();
    jdbcTemplate
        .queryForList(selectSQL)
        .forEach(
            r -> {
              Country country = new Country();
              country.setId((int) r.get("id"));
              country.setCode((String) r.get("code"));
              country.setPrefix((String) r.get("prefix"));
              country.setName((String) r.get("name"));
              country.setCurrencyName((String) r.get("currencyName"));
              country.setCurrencyCode((String) r.get("currencyCode"));




                SubArea subArea = new SubArea();
                subArea.setName((String) r.get("subArea"));
                subArea.setIataAreaCode((Short) r.get("iataAreaCode"));
                country.setSubArea(subArea);

              countries.add(country);
            });

    return countries;
  }
}
