package myapp.repo.interfaces;

import myapp.model.Country;
import myapp.model.SubArea;

import java.util.List;

public interface CountryRepo {
    void save(Country country);
    void update(Country country);
    List<Country>getDictionary();
    List<Country>get();
}
