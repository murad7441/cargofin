package myapp.model;

public class Airplane {
    private long id;
    private String name;
    private String ICAO;
    private String IATA;
    private float MTOW;
    private int MLW;
    private int MZFW;
    private  int weight;
    private int maxWidth;
    private  int maxLength;
    private boolean wideBody;
    private String editedBy;
    private String editedDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getICAO() {
        return ICAO;
    }

    public void setICAO(String ICAO) {
        this.ICAO = ICAO;
    }

    public String getIATA() {
        return IATA;
    }

    public void setIATA(String IATA) {
        this.IATA = IATA;
    }

    public float getMTOW() {
        return MTOW;
    }

    public void setMTOW(float MTOW) {
        this.MTOW = MTOW;
    }

    public int getMLW() {
        return MLW;
    }

    public void setMLW(int MLW) {
        this.MLW = MLW;
    }

    public int getMZFW() {
        return MZFW;
    }

    public void setMZFW(int MZFW) {
        this.MZFW = MZFW;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getMaxWidth() {
        return maxWidth;
    }

    public void setMaxWidth(int maxWidth) {
        this.maxWidth = maxWidth;
    }

    public int getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }

    public boolean isWideBody() {
        return wideBody;
    }

    public void setWideBody(boolean wideBody) {
        this.wideBody = wideBody;
    }

    public String getEditedBy() {
        return editedBy;
    }

    public void setEditedBy(String editedBy) {
        this.editedBy = editedBy;
    }

    public String getEditedDate() {
        return editedDate;
    }

    public void setEditedDate(String editedDate) {
        this.editedDate = editedDate;
    }
}
