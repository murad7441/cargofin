package myapp.model;

import java.time.LocalDateTime;

public class Country {
  private Integer id;
  private String code;
  private String prefix;
  private String name;
  private String currencyCode;
  private String currencyName;
  private String editedBy;
  private LocalDateTime editedDate;
  private SubArea subArea;

  public Country() {}

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getPrefix() {
    return prefix;
  }

  public void setPrefix(String prefix) {
    this.prefix = prefix;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public String getCurrencyName() {
    return currencyName;
  }

  public void setCurrencyName(String currencyName) {
    this.currencyName = currencyName;
  }

  public String getEditedBy() {
    return editedBy;
  }

  public void setEditedBy(String editedBy) {
    this.editedBy = editedBy;
  }

  public LocalDateTime getEditedDate() {
    return editedDate;
  }

  public void setEditedDate(LocalDateTime editedDate) {
    this.editedDate = editedDate;
  }

  public SubArea getSubArea() {
    return subArea;
  }

  public void setSubArea(SubArea subArea) {
    this.subArea = subArea;
  }
}
